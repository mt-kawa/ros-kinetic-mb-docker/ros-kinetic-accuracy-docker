#include "pointmatcher/PointMatcher.h"
#include "PcdIO.h"

class ICPPerformer {
	public:
		ICPPerformer();
		PointMatcher<float>::TransformationParameters PerformICP(std::string reference_scan_string);
};
