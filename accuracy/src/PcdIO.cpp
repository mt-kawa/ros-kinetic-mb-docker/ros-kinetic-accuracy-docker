#include "PcdIO.h"

PcdIO::PcdIO() {
}

Eigen::Vector4f PcdIO::TransformReferenceOriginTo(std::string fixed_frame, Eigen::Affine3d transform)
{
  Eigen::Vector4f v = Eigen::Vector4f::Zero();
  v.head<3>() = transform.translation().cast<float>();
  return v;
}

Eigen::Quaternionf PcdIO::TransformReferenceOrientationTo(std::string fixed_frame, Eigen::Affine3d transform)
{
  Eigen::Quaternionf q = Eigen::Quaternionf::Identity();
  q = transform.rotation().cast<float>();
  return q;
}

std::string PcdIO::SaveCloudPcd(pcl::PCLPointCloud2 cloud)
{
  pcl::PCDWriter writer;
  std::stringstream ss;
  ss << cloud.header.stamp << ".pcd";
  // Transform to fixed_frame e.g. /map needed?
  std::string fixed_frame = cloud.header.frame_id.c_str();
  if (!fixed_frame.empty()) {
    // if (!tf_buffer_.canTransform (fixed_frame, cloud.header.frame_id, pcl_conversions::fromPCL (cloud.header.stamp), ros::Duration (3.0))) {
      // ROS_WARN("Could not get transform!");
      // return 2;
    Eigen::Affine3d transform;
    // transform = tf2::transformToEigen(tf_buffer_.lookupTransform(fixed_frame, cloud.header.frame_id, pcl_conversions::fromPCL(cloud.header.stamp)));
    Eigen::Vector4f origin_vector = TransformReferenceOriginTo(fixed_frame, transform);
    Eigen::Quaternionf orientation_quaternion = TransformReferenceOrientationTo(fixed_frame, transform);
    writer.writeASCII(ss.str(), cloud, origin_vector, orientation_quaternion, 8);
    return ss.str().c_str();
  }
  return "No fixed_frame";
}

std::string PcdIO::SaveReferenceCloudPcd(std::string cloud)
{
  std::string cloud_file_name;
  cloud_file_name = "reference.pcd";
  std::ofstream out(cloud_file_name);
  out << cloud;
  out.close();
  return cloud_file_name;
}

std::string PcdIO::ReadPcdAsString(std::string file_name)
{
  std::ifstream ifs(file_name);
  std::string pcd_string(
      (std::istreambuf_iterator<char>(ifs)),
      (std::istreambuf_iterator<char>())
      );
  return pcd_string;
}

std::string PcdIO::GetPcdReading() {
  ros::NodeHandle client_nodehandle;
  ros::service::waitForService("assemble_scans2");
  ROS_INFO("Found assemble_scans2! Starting the assembler service");
  ros::ServiceClient client = client_nodehandle.serviceClient<AssembleScans2>("assemble_scans2");
  pcl::PCLPointCloud2 cloud;
  IntervalGenerator ig;
  AssembleScans2 srv = ig.GetSecondIntervalFromCurrentTime(0.25);

  try {
    if(client.call(srv)) {
      sensor_msgs::PointCloud2 ros_cloud = srv.response.cloud;
      pcl_conversions::toPCL(ros_cloud, cloud);
      ROS_INFO("Got cloud with %d data points in frame %s with those fields %s",
	cloud.width * cloud.height,
	cloud.header.frame_id.c_str(),
	pcl::getFieldsList(cloud).c_str());
      PcdIO io;
      std::string file_name_reading = io.SaveCloudPcd(cloud);
      return file_name_reading;
    }
  }
  catch (int e) {
    ROS_ERROR("Could not get reading scan - Error Nr. %i", e);
  }
}
