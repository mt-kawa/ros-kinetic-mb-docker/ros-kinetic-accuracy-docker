#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <Eigen/Geometry>
#include <fstream>

using namespace laser_assembler;
using namespace std;


class PcdIO {
	public:
		PcdIO();
		Eigen::Vector4f transformReferenceOriginTo(std::string fixed_frame, Eigen::Affine3d transform);
		Eigen::Quaternionf transformReferenceOrientationTo(std::string fixed_frame, Eigen::Affine3d transform);
		std::string saveCloudPcd(pcl::PCLPointCloud2 cloud);
		std::string readPcdAsString(std::string file_name);
};
