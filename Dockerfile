## Dockerfile for creating a ROS kinetic Node in C++11 for using the libpointmatcher libraries
##
## Author: Kai Waelti <Kai.Waelti@dfki.de

FROM ros:kinetic AS base

MAINTAINER Kai Waelti <Kai.Waelti@dfki.de>

ARG PCL_VERSION=1.9.1

RUN apt-get update && \
    apt-get install -y \
    build-essential \
    libboost-all-dev \
    libeigen3-dev \
    libflann1.8 libflann-dev \
    libpcl1.7 libpcl-dev \
    python-rosdep \
    python-catkin-tools \
    ros-kinetic-move-base-msgs \
    ros-kinetic-actionlib \
    ros-kinetic-laser-assembler \
    ros-kinetic-pcl-ros \
    libyaml-cpp-dev

FROM base as dependencies

## Install libnabo for NN searches
RUN mkdir /libraries && \
    cd /libraries && \
    git clone git://github.com/ethz-asl/libnabo.git && \
    cd libnabo && \
    SRC_DIR=`pwd` && \
    BUILD_DIR=${SRC_DIR}/build && \
    mkdir -p ${BUILD_DIR} && \
    cd ${BUILD_DIR} && \
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ${SRC_DIR} && \
    make && make install

## Install libpointmatcher
RUN cd /libraries && \
    git clone git://github.com/ethz-asl/libpointmatcher.git && \
    cd libpointmatcher && \
    SRC_DIR=`pwd` && \
    BUILD_DIR=${SRC_DIR}/build && \
    mkdir -p ${BUILD_DIR} && cd ${BUILD_DIR} && \
    ln -s eigen/Eigen /usr/local/include && \
    cmake -D CMAKE_BUILD_TYPE=RelWithDebInfo ${SRC_DIR} && \
    make && \
    make install

FROM dependencies as app

COPY accuracy /ws/src/accuracy

WORKDIR /ws

SHELL ["/bin/bash", "-c"]	# Change to bash shell for ros stuff

RUN source /opt/ros/kinetic/setup.bash && \
    catkin init && \
    catkin config --install -j 2 && \
    catkin_make

COPY launch-files /launch-files
COPY run-shells /run-shells

WORKDIR /root
COPY entrypoint.sh .

ENTRYPOINT ["/root/entrypoint.sh"]

ENV ROS_MASTER_URI "http://ros-master:11311"

CMD ["bash"]

