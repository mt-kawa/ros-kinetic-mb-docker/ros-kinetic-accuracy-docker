#include <ros/ros.h>
#include <laser_assembler/AssembleScans2.h>

using namespace laser_assembler;

class IntervalGenerator {
	public:
		IntervalGenerator();
		AssembleScans2 GetSecondIntervalFromCurrentTime(double scan_seconds);

	private:
		AssembleScans2 srv;
};
