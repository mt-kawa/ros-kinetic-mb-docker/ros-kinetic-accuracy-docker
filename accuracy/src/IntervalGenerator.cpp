#include "IntervalGenerator.h"

IntervalGenerator::IntervalGenerator() {
}

AssembleScans2 IntervalGenerator::GetSecondIntervalFromCurrentTime(double scan_seconds)
{
	AssembleScans2 srv;
  // For simulation - wait for ROS Time
  while (ros::Time::now() == ros::Time(0.0)) {
    ros::Duration(1).sleep();
  }
  ROS_INFO("Got ROS Time: %d\n", srv.request.end);
  // Determine time interval of last second using current time
  ros::Time last_time = srv.response.cloud.header.stamp;
  srv.request.begin = ros::Time::now() - ros::Duration(scan_seconds);
  srv.request.end   = ros::Time::now();
  return srv;
}
