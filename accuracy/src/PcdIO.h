#include <ros/ros.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <laser_assembler/AssembleScans2.h>
#include <sensor_msgs/PointCloud2.h>
#include <Eigen/Geometry>
#include <fstream>
#include "IntervalGenerator.h"

using namespace std;


class PcdIO {
	public:
		PcdIO();
		Eigen::Vector4f TransformReferenceOriginTo(std::string fixed_frame, Eigen::Affine3d transform);
		Eigen::Quaternionf TransformReferenceOrientationTo(std::string fixed_frame, Eigen::Affine3d transform);
		std::string SaveCloudPcd(pcl::PCLPointCloud2 cloud);
		std::string SaveReferenceCloudPcd(std::string cloud);
		std::string ReadPcdAsString(std::string file_name);
		std::string GetPcdReading();
};
