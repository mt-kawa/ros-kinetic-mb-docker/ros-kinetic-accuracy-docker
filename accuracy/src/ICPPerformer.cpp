#include "ICPPerformer.h"

using namespace std;

ICPPerformer::ICPPerformer() {
}

PointMatcher<float>::TransformationParameters ICPPerformer::PerformICP(std::string reference_scan_string) {
  typedef PointMatcher<float> PM;
  typedef PM::DataPoints DP;
  //
  // // Load point clouds
  PcdIO io;
  std::string reference_fn = io.SaveReferenceCloudPcd(reference_scan_string);
  std::string reading_fn = io.GetPcdReading();
  ROS_INFO("Point clouds saved");
  const DP reference_dp(DP::load(reference_fn));
  const DP reading_dp(DP::load(reading_fn));
  // Create the default ICP algorithm
  PM::ICP icp;
  PointMatcherSupport::Parametrizable::Parameters params;
  std::string name;

  setLogger(PM::get().LoggerRegistrar.create("FileLogger"));

  // Prepare reading filters
  name = "MinDistDataPointsFilter";
  params["minDist"] = "1.0";
  std::shared_ptr<PM::DataPointsFilter> minDist_read =
	  PM::get().DataPointsFilterRegistrar.create(name, params);
  params.clear();

  name = "RandomSamplingDataPointsFilter";
  params["prob"] = "0.05";
  std::shared_ptr<PM::DataPointsFilter> rand_read =
	  PM::get().DataPointsFilterRegistrar.create(name, params);
  params.clear();

  // Prepare reference filters
  name = "MinDistDataPointsFilter";
  params["minDist"] = "1.0";
  std::shared_ptr<PM::DataPointsFilter> minDist_ref =
	  PM::get().DataPointsFilterRegistrar.create(name, params);
  params.clear();

  name = "RandomSamplingDataPointsFilter";
  params["prob"] = "0.05";
  std::shared_ptr<PM::DataPointsFilter> rand_ref =
	  PM::get().DataPointsFilterRegistrar.create(name, params);
  params.clear();

  // Prepare matching function
  name = "KDTreeMatcher";
  params["knn"] = "1";
  params["epsilon"] = "3.16";
  std::shared_ptr<PM::Matcher> kdtree =
	  PM::get().MatcherRegistrar.create(name, params);
  params.clear();

  // Prepare outlier filters
  name = "TrimmedDistOutlierFilter";
  params["ratio"] = "0.75";
  std::shared_ptr<PM::OutlierFilter> trim =
	  PM::get().OutlierFilterRegistrar.create(name, params);
  params.clear();

  // Prepare error minimization
  name = "PointToPointErrorMinimizer";
  std::shared_ptr<PM::ErrorMinimizer> pointToPoint =
	  PM::get().ErrorMinimizerRegistrar.create(name);

  // Prepare transformation checker filters
  name = "CounterTransformationChecker";
  params["maxIterationCount"] = "150";
  std::shared_ptr<PM::TransformationChecker> maxIter =
	  PM::get().TransformationCheckerRegistrar.create(name, params);
  params.clear();

  name = "DifferentialTransformationChecker";
  params["minDiffRotErr"] = "0.001";
  params["minDiffTransErr"] = "0.01";
  params["smoothLength"] = "4";
  std::shared_ptr<PM::TransformationChecker> diff =
	  PM::get().TransformationCheckerRegistrar.create(name, params);
  params.clear();

  // Prepare inspector
  name = "VTKFileInspector";
  params["dumpDataLinks"] = "1";
  params["dumpReading"] = "1";
  params["dumpReference"] = "1";
  std::shared_ptr<PM::Inspector> vtkInspect = PM::get().InspectorRegistrar.create(name, params);
  std::shared_ptr<PM::Inspector> nullInspect = PM::get().InspectorRegistrar.create("NullInspector");
  // toggle to write vtk files per iteration
  // icp.inspector = nullInspect;
  icp.inspector = vtkInspect;
  params.clear();

  std::shared_ptr<PM::Transformation> rigidTrans = PM::get().TransformationRegistrar.create("RigidTransformation");

  // Build ICP solution
  icp.readingDataPointsFilters.push_back(minDist_read);
  icp.readingDataPointsFilters.push_back(rand_read);

  icp.referenceDataPointsFilters.push_back(minDist_ref);
  icp.referenceDataPointsFilters.push_back(rand_ref);

  icp.matcher = kdtree;

  icp.outlierFilters.push_back(trim);

  icp.errorMinimizer = pointToPoint;

  icp.transformationCheckers.push_back(maxIter);
  icp.transformationCheckers.push_back(diff);

  // toggle to write vtk files per iteration
  icp.inspector = nullInspect;
  //icp.inspector = vtkInspect;

  icp.transformations.push_back(rigidTrans);

  // Compute the transformation to express data in ref
  PM::TransformationParameters T = icp(reading_dp, reference_dp);
  // Transform data to express it in ref
  DP data_out(reading_dp);
  icp.transformations.apply(data_out, T);
  // Save files to see the results
  ROS_INFO("Saving vtk files");
  reference_dp.save("test_ref.vtk");
  reading_dp.save("test_data_in.vtk");
  data_out.save("test_data_out.vtk");
  cout << "Final transformation:" << endl << T << endl;
  return T;
}
