#include <ros/ros.h>
#include "pointmatcher/PointMatcher.h"
#include <cassert>
#include <iostream>
#include <memory>
#include "boost/filesystem.hpp"
#include <tf/tf.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <math.h>

#include "accuracy/FineAdjustmentAtTarget.h"
#include "ICPPerformer.h"

using namespace std;
using namespace laser_assembler;

// Define a precise PI hoping that the C++ implementation produces a correctly
// rounded value
// constexpr evaluates that at compile time for optimization
constexpr double PI = std::acos(-1);
geometry_msgs::PoseStamped current_pose;
geometry_msgs::PoseStamped goal_pose;
ros::Publisher pub_pose2d_odom;
ros::Publisher pub_pose2d_goal;
double goal_x;
double goal_y;
double current_x;
double current_y;

void OdomCallback(const geometry_msgs::PoseStampedConstPtr& msg)
{
  current_pose.pose = msg->pose;
  current_pose.header.stamp = ros::Time::now();
  current_pose.header.frame_id = "/map";
  current_x = current_pose.pose.position.x;
  current_y = current_pose.pose.position.y;
  pub_pose2d_odom.publish(current_pose);
  // cout << "Odometry Pose \n" << current_pose;
}

void GoalposeCallback(const geometry_msgs::PoseStampedConstPtr& msg)
{
  goal_pose.pose = msg->pose;
  goal_pose.header.stamp = ros::Time::now();
  goal_pose.header.frame_id = "/map";
  goal_x = goal_pose.pose.position.x;
  goal_y = goal_pose.pose.position.y;
  pub_pose2d_goal.publish(goal_pose);
  cout << "Goal Pose \n" << goal_pose << endl;
}

bool AreSame(double a, double b)
{
  return std::abs(a - b) < 0.01;
}

bool AreSameAccurate(double a, double b)
{
  return std::abs(a - b) < 0.001;
}

bool DistanceMovedSmallerAsDifference(double moved_distance,
    double difference) {
  return std::abs(moved_distance) < std::abs(difference);
}

double GetAngleToGoal() {
  double inc_x = goal_x - current_x;
  double inc_y = goal_y - current_y;
  return std::atan2(inc_y, inc_x);
}

double GetPoseYaw(geometry_msgs::PoseStamped pose_stamped) {
  tf::Stamped<tf::Pose> pose_tf;
  tf::poseStampedMsgToTF(pose_stamped, pose_tf);
  return tf::getYaw(pose_tf.getRotation());
}

double GetOdomYaw() {
  return GetPoseYaw(current_pose);
}

double GetGoalYaw() {
  return GetPoseYaw(goal_pose);
}

double DriveXDistance(double distance_in_m, double speed) {
  ros::NodeHandle n;
  ros::Publisher movement_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
  ros::Rate rate(60); //the larger the value, the "smoother"
  double current_time;
  double current_distance = 0;
  double start_time = ros::Time::now().toSec();
  while (ros::ok() &&
      DistanceMovedSmallerAsDifference(current_distance, distance_in_m)) {
    geometry_msgs::Twist move;
    move.linear.y = 0.0;
    move.angular.z = 0.0;
    if (distance_in_m > 0) {
      move.linear.x = speed; //speed value m/s
    } else {
      move.linear.x = -speed; //speed value m/s
    }
    movement_pub.publish(move);
    current_time = ros::Time::now().toSec();
    current_distance = move.linear.x*(current_time-start_time);
    cout << "Moving by \r" << move;
    cout << "Current distance " << current_distance;
    cout << "/" << distance_in_m;
    cout.flush();
    ros::spinOnce();
    rate.sleep();
  }
  cout << endl;
  return current_distance;
}

bool MoveBasedOnOdom() {
  ros::NodeHandle n;
  ros::Publisher movement_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
  ros::Rate rate(60); //the larger the value, the "smoother"
  ROS_INFO("Start move based on /robot_pose and /goal_pose");
  ros::Time start_time = ros::Time::now();

  double odom_yaw = GetOdomYaw();
  double theta = GetAngleToGoal();

  // vector<double> start_to_goal_vector = {goal_x - current_x, goal_y - current_y};

  double goal_yaw = GetGoalYaw();
  double cos_of_theta = cos(theta);
  double odom_theta_diff = abs(odom_yaw - theta);
  double speed = 0.05;
  double drive_back_distance_m = 0.2;
  ROS_INFO("Calculated orientation to goal pose");

  // If the robot (odometry) orientation is not towards the target, then rotate
  // only the robot until its orientation is towards the target
  // 3 cases:
  // 1. cosine < 0 -> odom behind goal
  // 2. cosine > 0 -> goal behind odom
  // 3. cosine = 0 -> goal and odom same height -> drive back 5 cm and do case 1
  while (ros::ok()
    && (!AreSameAccurate(theta, odom_yaw)))
  {
    geometry_msgs::Twist move;
    theta = GetAngleToGoal();
    odom_yaw = GetOdomYaw();
    odom_theta_diff = abs(odom_yaw - theta);
    cout << "Odom_yaw is " << odom_yaw << endl;
    cout << "Odom_theta_diff is " << odom_theta_diff << endl;
    if (odom_theta_diff < PI/3) {
      cout << "Difference in orientations small enough" << endl;
      // Is goal to the left?
      if (theta > odom_yaw) {
	move.angular.z = speed; // CCW rotation
      } else {
	move.angular.z = -speed; // CW rotation
      }
    } else {
      cout << "We are to the side or in front of the goal " << theta << endl;
      cout << "Driving back " << drive_back_distance_m << endl;
      DriveXDistance(-drive_back_distance_m, 0.2);
    }
    movement_pub.publish(move);
    ros::spinOnce();
    rate.sleep();
  }

  // Once the robot is headed towards the target, just move straight towards the
  // goal until reached
  while (ros::ok() &&
      !AreSameAccurate(current_pose.pose.position.x, goal_pose.pose.position.x)
      && !AreSameAccurate(current_pose.pose.position.y, goal_pose.pose.position.y)) {
    geometry_msgs::Twist move;
    move.linear.x = speed;
    movement_pub.publish(move);
    ros::spinOnce();
    rate.sleep();
  }

  // Final rotation
  cout << "Final pose rotation from " << odom_yaw;
  cout << "to " << goal_yaw << endl;
  while (ros::ok() &&
	  !AreSameAccurate(odom_yaw, goal_yaw)) {
    goal_yaw = GetGoalYaw();
    odom_yaw = GetOdomYaw();
    geometry_msgs::Twist move;
    if (goal_yaw > odom_yaw) {
      move.angular.z = speed;
    } else {
      move.angular.z = -speed;
    }
    movement_pub.publish(move);
    cout << "Moving by \r" << move;
    ros::spinOnce();
    rate.sleep();
  }

  ros::Time end_time = ros::Time::now();
  ros::Duration diff=end_time-start_time;
  cout << "Finished Odom based fine adjustment. Took " << diff << endl;

  // just stop
  geometry_msgs::Twist move;
  move.linear.x = 0.0;
  move.linear.y = 0.0;
  move.angular.z = 0.0;
  movement_pub.publish(move);
  ros::spinOnce();
  rate.sleep();
}

bool FineAdjustmentAtTarget(accuracy::FineAdjustmentAtTarget::Request &request,
    accuracy::FineAdjustmentAtTarget::Response &response)
{
  MoveBasedOnOdom();
  ros::Duration(1).sleep();
  std::string reference_scan_string = request.reference_scan;
  // Perform ICP for final pose accuracy estimation
  ICPPerformer icpPerformer;
  PointMatcher<float>::TransformationParameters icpTransformation = icpPerformer.PerformICP(reference_scan_string);
  std::ostringstream output_status;
  output_status << "Final pose accuracy estimation\n" << icpTransformation;
  std::string output_status_string = output_status.str();
  response.status = output_status_string;
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "icp_subroutine");
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService("/accuracy/fine_adjustment_at_target", FineAdjustmentAtTarget);
  pub_pose2d_odom = n.advertise<geometry_msgs::PoseStamped>("/accuracy/mb_fine_adjust_odom_pose", 1);
  pub_pose2d_goal = n.advertise<geometry_msgs::PoseStamped>("/accuracy/mb_fine_adjust_goal_pose", 1);
  ROS_INFO("Ready to start iterating...");
  ros::Subscriber sub_goalpose = n.subscribe("/move_base/current_goal", 1, GoalposeCallback);
  ros::Subscriber sub_odompose = n.subscribe("/robot_pose", 1, OdomCallback);
  ros::spin();

  return 0;
}
