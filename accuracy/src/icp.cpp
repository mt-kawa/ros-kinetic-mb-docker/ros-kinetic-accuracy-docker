#include <ros/ros.h>
#include "pointmatcher/PointMatcher.h"
#include <cassert>
#include <iostream>
#include <memory>
#include "boost/filesystem.hpp"
#include <tf/tf.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <math.h>

#include "accuracy/FineAdjustmentAtTarget.h"
#include "ICPPerformer.h"

using namespace std;
using namespace laser_assembler;

// Define a precise PI hoping that the C++ implementation produces a correctly
// rounded value
// constexpr evaluates that at compile time for optimization
constexpr double PI = std::acos(-1);
geometry_msgs::PoseStamped current_pose;
geometry_msgs::PoseStamped goal_pose;
ros::Publisher pub_pose2d_odom;
ros::Publisher pub_pose2d_goal;
double icp_linear_speed = 0.005;
double icp_angular_speed = 0.01;
double goal_x;
double goal_y;
double current_x;
double current_y;

void OdomCallback(const geometry_msgs::PoseStampedConstPtr& msg)
{
  current_pose.pose = msg->pose;
  current_pose.header.stamp = ros::Time::now();
  current_pose.header.frame_id = "/map";
  current_x = current_pose.pose.position.x;
  current_y = current_pose.pose.position.y;
  pub_pose2d_odom.publish(current_pose);
  // cout << "Odometry Pose \n" << current_pose;
}

void GoalposeCallback(const geometry_msgs::PoseStampedConstPtr& msg)
{
  goal_pose.pose = msg->pose;
  goal_pose.header.stamp = ros::Time::now();
  goal_pose.header.frame_id = "/map";
  goal_x = goal_pose.pose.position.x;
  goal_y = goal_pose.pose.position.y;
  pub_pose2d_goal.publish(goal_pose);
  cout << "Goal Pose \n" << goal_pose << endl;
}

bool AreSame(double a, double b)
{
  return std::abs(a - b) < 0.01;
}

bool AreSameAccurate(double a, double b)
{
  return std::abs(a - b) < 0.001;
}

bool DistanceMovedSmallerAsDifference(double moved_distance,
    double difference) {
  return std::abs(moved_distance) < std::abs(difference);
}

double GetAngleToGoal() {
  double inc_x = goal_x - current_x;
  double inc_y = goal_y - current_y;
  return std::atan2(inc_y, inc_x);
}

double GetPoseYaw(geometry_msgs::PoseStamped pose_stamped) {
  tf::Stamped<tf::Pose> pose_tf;
  tf::poseStampedMsgToTF(pose_stamped, pose_tf);
  return tf::getYaw(pose_tf.getRotation());
}

double GetOdomYaw() {
  return GetPoseYaw(current_pose);
}

double GetGoalYaw() {
  return GetPoseYaw(goal_pose);
}

double DriveXDistance(double distance_in_m, double speed) {
  ros::NodeHandle n;
  ros::Publisher movement_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
  ros::Rate rate(60); //the larger the value, the "smoother"
  double current_time;
  double current_distance = 0;
  double start_time = ros::Time::now().toSec();
  while (ros::ok() &&
      DistanceMovedSmallerAsDifference(current_distance, distance_in_m)) {
    geometry_msgs::Twist move;
    move.linear.y = 0.0;
    move.angular.z = 0.0;
    if (distance_in_m > 0) {
      move.linear.x = -speed; //speed value m/s
    } else {
      move.linear.x = speed; //speed value m/s
    }
    movement_pub.publish(move);
    current_time = ros::Time::now().toSec();
    current_distance = move.linear.x*(current_time-start_time);
    cout << "Moving by \r" << move;
    cout << "Current distance " << current_distance;
    cout << "/" << distance_in_m;
    cout.flush();
    ros::spinOnce();
    rate.sleep();
  }
  cout << endl;
  return current_distance;
}

double DriveYDistance(double distance_in_m, double speed) {
  ros::NodeHandle n;
  ros::Publisher movement_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
  ros::Rate rate(60); //the larger the value, the "smoother"
  double current_time;
  double current_distance = 0;
  double start_time = ros::Time::now().toSec();
  while (ros::ok() &&
      DistanceMovedSmallerAsDifference(current_distance, distance_in_m)) {
    geometry_msgs::Twist move;
    move.linear.x = 0.0;
    move.angular.z = 0.0;
    if (distance_in_m > 0) {
      move.linear.y = -speed; //speed value m/s
    } else {
      move.linear.y = speed; //speed value m/s
    }
    movement_pub.publish(move);
    current_time = ros::Time::now().toSec();
    current_distance = move.linear.y*(current_time-start_time);
    cout << "Moving by \r" << move;
    cout << "Current distance " << current_distance;
    cout << "/" << distance_in_m;
    cout.flush();
    ros::spinOnce();
    rate.sleep();
  }
  cout << endl;
  return current_distance;
}

double TurnZRadians(double z_rad, double speed_in_deg_per_sec) {
  ros::NodeHandle n;
  ros::Publisher movement_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
  ros::Rate rate(60); //the larger the value, the "smoother"
  double current_time;
  double current_angle = 0;
  double start_time = ros::Time::now().toSec();
  double angular_speed = speed_in_deg_per_sec*2*PI/360;
  while (ros::ok() &&
      DistanceMovedSmallerAsDifference(current_angle, z_rad)) {
    geometry_msgs::Twist move;
    move.linear.x = 0.0;
    move.linear.y = 0.0;
    if (z_rad > 0) {
      move.angular.z = -angular_speed;
    } else {
      move.angular.z = angular_speed;
    }
    movement_pub.publish(move);
    current_time = ros::Time::now().toSec();
    current_angle = move.angular.z*(current_time-start_time);
    cout << "Moving by \r" << move;
    cout << "Current distance " << current_angle;
    cout << "/" << z_rad;
    cout.flush();
    ros::spinOnce();
    rate.sleep();
  }
  cout << endl;
  return current_angle;
}

bool MoveBasedOnOdom() {
  ros::NodeHandle n;
  ros::Publisher movement_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
  ros::Rate rate(60); //the larger the value, the "smoother"
  ROS_INFO("Start move based on /robot_pose and /goal_pose");
  ros::Time start_time = ros::Time::now();

  double odom_yaw = GetOdomYaw();
  double theta = GetAngleToGoal();
  double goal_yaw = GetGoalYaw();
  double speed = 0.05;
  double drive_back_distance_m = 0.05;
  ROS_INFO("Calculated orientation to goal pose");

  // Compute cos(theta) and turn towards goal pose by minimizing amount of
  // rotation needed
  double cos_of_theta = cos(theta);

  // If the robot (odometry) orientation is not towards the target, then rotate
  // only the robot until its orientation is towards the target
  // 3 cases:
  // 1. cosine < 0 -> odom behind goal
  // 2. cosine > 0 -> goal behind odom
  // 3. cosine = 0 -> goal and odom same height -> drive back 5 cm and do case 1
  while (ros::ok()
    && !AreSameAccurate(theta, odom_yaw))
  {
    if (cos_of_theta < 0) {
      cout << "We are behind goal with " << cos_of_theta << endl;
      theta = GetAngleToGoal();
      odom_yaw = GetOdomYaw();
      geometry_msgs::Twist move;
      move.linear.x = 0.0; //speed value m/s
      move.linear.y = 0.0;
      // Is goal to the left?
      cout << "Theta is " << theta;
      cout << "Odom_yaw is " << odom_yaw;
      if (theta > odom_yaw) {
	move.angular.z = speed; // CCW rotation
      } else {
	move.angular.z = -speed; // CW rotation
      }
      movement_pub.publish(move);
      cout << "Moving by \n" << move;
      ros::spinOnce();
      rate.sleep();
    } else if (cos_of_theta > 0) {
      cout << "We are in front of goal with " << cos_of_theta << endl;
    } else {
      cout << "We are at same height with goal" << cos_of_theta << endl;
      cout << "Driving back " << drive_back_distance_m << endl;
      DriveXDistance(drive_back_distance_m, 0.4);
      cos_of_theta = cos(theta);
    }
  }

  // Once the robot is headed towards the target, just move straight towards the
  // goal until reached
  cout << "Move in x direction " << cos_of_theta << endl;
  while (ros::ok() &&
      !AreSameAccurate(current_pose.pose.position.x, goal_pose.pose.position.x)) {
    theta = GetAngleToGoal();
    cos_of_theta = cos(theta);
    geometry_msgs::Twist move;
    move.linear.y = 0.0; //speed value m/s
    move.angular.z = 0.0;
    // Is goal in front?
    if (cos_of_theta < 0) {
      move.linear.x = speed;
    } else {
      move.linear.x = -speed;
    }
    movement_pub.publish(move);
    cout << "Moving by \n" << move;
    ros::spinOnce();
    rate.sleep();
  }

  // Final rotation
  cout << "Final pose rotation from " << odom_yaw;
  cout << "to " << goal_yaw;
  while (ros::ok() &&
	  !AreSameAccurate(odom_yaw, goal_yaw)) {
    goal_yaw = GetGoalYaw();
    odom_yaw = GetOdomYaw();
    geometry_msgs::Twist move;
    move.linear.x = 0.0; //speed value m/s
    move.linear.y = 0.0;
    if (goal_yaw > odom_yaw) {
      move.angular.z = speed;
    } else {
      move.angular.z = -speed;
    }
    movement_pub.publish(move);
    cout << "Moving by \r" << move;
    ros::spinOnce();
    rate.sleep();
  }

  ros::Time end_time = ros::Time::now();
  ros::Duration diff=end_time-start_time;
  cout << "Finished Odom based fine adjustment. Took " << diff << endl;

  // just stop
  geometry_msgs::Twist move;
  move.linear.x = 0.0;
  move.linear.y = 0.0;
  move.angular.z = 0.0;
  movement_pub.publish(move);
  ros::spinOnce();
  rate.sleep();
}

bool MoveBasedOnICP(PointMatcher<float>::TransformationParameters icpTransform) {
  ROS_INFO("Start move based on ICP transform");

  double x_diff = icpTransform.coeff(0, 3);
  double y_diff = icpTransform.coeff(1, 3);
  double z_rad = icpTransform.coeff(0, 2);
  cout << "ICP translation vector" << icpTransform.rightCols(1);
  cout << "ICP rotation matrix" << icpTransform.topLeftCorner(2,2);
  cout << "ICP x_diff" << x_diff;
  cout << "ICP y_diff" << y_diff;

  // Correct X
  DriveXDistance(x_diff, icp_linear_speed);
  // Correct Y
  DriveYDistance(y_diff, icp_linear_speed);
  // Correct Angle
  TurnZRadians(z_rad, icp_angular_speed);
}

bool FineAdjustmentAtTarget(accuracy::FineAdjustmentAtTarget::Request &request,
    accuracy::FineAdjustmentAtTarget::Response &response)
{
  // MoveBasedOnOdom();
  // ros::Duration(1).sleep();
  std::string reference_scan_string = request.reference_scan;
  ICPPerformer icpPerformer;
  // Perform ICP with fine adjustment
  PointMatcher<float>::TransformationParameters icpTransformation = icpPerformer.PerformICP(reference_scan_string);
  MoveBasedOnICP(icpTransformation);
  // Perform ICP for final pose accuracy estimation
  ros::Duration(1).sleep();
  icpTransformation = icpPerformer.PerformICP(reference_scan_string);
  MoveBasedOnICP(icpTransformation);
  std::ostringstream output_status;
  icpTransformation = icpPerformer.PerformICP(reference_scan_string);
  output_status << "Final pose accuracy estimation\n" << icpTransformation;
  std::string output_status_string = output_status.str();
  response.status = output_status_string;
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "icp_subroutine");
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService("/accuracy/fine_adjustment_at_target", FineAdjustmentAtTarget);
  pub_pose2d_odom = n.advertise<geometry_msgs::PoseStamped>("/accuracy/mb_fine_adjust_odom_pose", 1);
  pub_pose2d_goal = n.advertise<geometry_msgs::PoseStamped>("/accuracy/mb_fine_adjust_goal_pose", 1);
  ROS_INFO("Ready to start iterating...");
  ros::Subscriber sub_goalpose = n.subscribe("/move_base/current_goal", 1, GoalposeCallback);
  ros::Subscriber sub_odompose = n.subscribe("/robot_pose", 1, OdomCallback);
  ros::spin();

  return 0;
}
