#include <ros/ros.h>
#include <laser_assembler/AssembleScans2.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>
// PCL includes
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <Eigen/Geometry>
#include <fstream>

#include "accuracy/CreateReferenceScan.h"
#include "PcdIO.h"

using namespace laser_assembler;
using namespace std;

bool CreateReferenceScan(accuracy::CreateReferenceScan::Request &request,
    accuracy::CreateReferenceScan::Response &response)
{
  ros::NodeHandle client_nodehandle;
  ros::service::waitForService("assemble_scans2");
  ROS_INFO("Found assemble_scans2! Starting the assembler service");
  ros::ServiceClient client = client_nodehandle.serviceClient<AssembleScans2>("assemble_scans2");
  pcl::PCLPointCloud2 cloud;
  IntervalGenerator ig;
  AssembleScans2 srv = ig.GetSecondIntervalFromCurrentTime(request.scan_seconds);

  // Call service and convert cloud to PCL cloud for saving as ASCII string
  if(client.call(srv)) {
    sensor_msgs::PointCloud2 ros_cloud = srv.response.cloud;
    pcl_conversions::toPCL(ros_cloud, cloud);
    if((cloud.width * cloud.height) == 0) {
      return false;
    }

    ROS_INFO("Got cloud with %d data points in frame %s with those fields %s",
        cloud.width * cloud.height,
        cloud.header.frame_id.c_str(),
        pcl::getFieldsList(cloud).c_str());

    PcdIO io;
    std::string file_name = io.SaveCloudPcd(cloud);
    ROS_INFO("Data saved to %s", file_name);
    response.scan = io.ReadPcdAsString(file_name);
    ROS_INFO("Read PCD file");
    return true;
  }
  else {
    ROS_ERROR("Service call failed");
    return false;
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "laser_to_pcd_string_assembler");
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService("/accuracy/create_reference_scan", CreateReferenceScan);
  ROS_INFO("Ready to create a reference scan");
  ros::spin();

  return 0;
}
