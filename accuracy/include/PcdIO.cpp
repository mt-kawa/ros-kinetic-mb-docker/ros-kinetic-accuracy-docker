#include "PcdIO.h"

PcdIO::PcdIO() {
}

Eigen::Vector4f transformReferenceOriginTo(std::string fixed_frame, Eigen::Affine3d transform)
{
  Eigen::Vector4f v = Eigen::Vector4f::Zero();
  v.head<3>() = transform.translation().cast<float>();
  return v;
}

Eigen::Quaternionf transformReferenceOrientationTo(std::string fixed_frame, Eigen::Affine3d transform)
{
  Eigen::Quaternionf q = Eigen::Quaternionf::Identity();
  q = transform.rotation().cast<float>();
  return q;
}

std::string saveCloudPcd(pcl::PCLPointCloud2 cloud)
{
  pcl::PCDWriter writer;
  std::stringstream ss;
  ss << cloud.header.stamp << ".pcd";
  // Transform to fixed_frame e.g. /map needed?
  std::string fixed_frame = cloud.header.frame_id.c_str();
  if (!fixed_frame.empty()) {
    // if (!tf_buffer_.canTransform (fixed_frame, cloud.header.frame_id, pcl_conversions::fromPCL (cloud.header.stamp), ros::Duration (3.0))) {
      // ROS_WARN("Could not get transform!");
      // return 2;
    Eigen::Affine3d transform;
    // transform = tf2::transformToEigen(tf_buffer_.lookupTransform(fixed_frame, cloud.header.frame_id, pcl_conversions::fromPCL(cloud.header.stamp)));
    Eigen::Vector4f origin_vector = transformReferenceOriginTo(fixed_frame, transform);
    Eigen::Quaternionf orientation_quaternion = transformReferenceOrientationTo(fixed_frame, transform);
    writer.writeASCII(ss.str(), cloud, origin_vector, orientation_quaternion, 8);
    return ss.str().c_str();
  }
  return "No fixed_frame";
}

std::string readPcdAsString(std::string file_name)
{
  std::ifstream ifs(file_name);
  std::string pcd_string(
      (std::istreambuf_iterator<char>(ifs)),
      (std::istreambuf_iterator<char>())
      );
  return pcd_string;
}
