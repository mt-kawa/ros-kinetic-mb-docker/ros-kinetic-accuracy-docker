#include <ros/ros.h>
#include "geometry_msgs/PoseStamped.h"

#include "accuracy/GetRobotPose.h"

using namespace std;


bool GetRobotPose(accuracy::GetRobotPose::Request &request,
    accuracy::GetRobotPose::Response &response)
{
  ros::NodeHandle client_nodehandle;
  const geometry_msgs::PoseStampedConstPtr& robot_pose_msg =
    ros::topic::waitForMessage<geometry_msgs::PoseStamped>(
        "/robot_pose",
        client_nodehandle);
  ROS_INFO("Received message from /robot_pose");
  geometry_msgs::PoseStamped robot_pose = *robot_pose_msg;
  response.pose = robot_pose.pose;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "get_robot_pose_service");
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService(
      "/accuracy/get_robot_pose",
      GetRobotPose);
  ROS_INFO("Ready to serve robot pose");
  ros::spin();

  return 0;
}
